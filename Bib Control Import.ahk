#SingleInstance Force
; In which we define files

csvDataFile := "C:\Temp\BibImport.csv"

; Let's get some kinda idea how long this will take
Loop, Read, %csvDataFile%
{
   total_lines = %A_Index%
}

; In which we build a GUI

lines_remain := total_lines
;percent_complete := (total_lines - lines_remain) / ((lines_remain + total_lines) / 2) * 100

Gui, Font, s14 bold, Arial
Gui, Add, Text,, Import in progress
Gui, Font, s10 norm, Arial
Gui, Add, Text,, Total Lines: %total_lines%
Gui, Add, Text,vRem, Lines Remaining: %lines_remain%
Gui, Add, Text,vPerc, Percentage Complete: 0.0000
Gui, Show, x300 y300 h200 w300, Bib Import Progress
Sleep, 2000


; Parse the data in the file
Loop, Read, %csvDataFile%

{
	Loop, Parse, A_LoopReadLine, CSV
 {
	if (A_Index = 1)
		bibnumber := A_LoopField
 }
 
	; Begin sending the data to the Polaris Record Set
	SetTitleMatchMode, 2
	WinActivate, Bibliographic Records - Control number Find Tool
	Send, !n
	Sleep, 500
	Send %bibnumber%
	Sleep, 500
	Send {ENTER}
	Sleep, 2000
	Send {ENTER}
	Sleep, 500
	
	; Update the maths
	lines_remain -= 1
	update_percent := Round(((total_lines - lines_remain) / total_lines) * 100)
	progbar := 100 - %update_percent%
	sleep, 500
	
	; Update the GUI
	GuiControl,,Rem, Lines Remaining: %lines_remain%
	GuiControl,,Perc, Percentage Complete: %update_percent%

	; Check to see if we're done yet
	If lines_remain = 0
		break
}

; Import completed
Gui, Destroy
MsgBox,, Bib Record Import, Import Complete
ExitApp