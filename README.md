# Bib Control Import

#### Requires:

* AutoHotkey
* Windows (7, 8, 10)
* Polaris ILS (Supported: 4.1R2 - 5.2. Should work with newer versions, but I've not tested it.)

---

Bib Control Input is a script written in AutoHotkey and automates the import of bibliographic control numbers into a record set in Polaris ILS. Polaris doesn't natively allow for importing via CSV or Excel files, so I wrote this little automation script to grab data from a CSV and import it via keystroke automation.

### To Use

Bib Control Input is set to work out of the C:\Temp directory. You can change that if needed. You'll need a CSV of bibliographic control numbers that you can acquire from SimplyReports or SQL. Place that in your Temp directory. Using the Polaris Staff Client, open the Bibliographic Record Find Tool and set it to search by control number. (Save that as default so it doesn't revert while the script runs.) Clear the search by clicking New Search, then double click the AHK script to kick it off.

I recommend standing by while the script runs. There's sufficient timing in the script to allow for lag and database slowness, but I always keep an eye on it while it works. Just in case.

### Questions?

cyberpunklibrarian (at) protonmail (dot) com